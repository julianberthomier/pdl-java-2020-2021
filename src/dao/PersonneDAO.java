package dao;
import java.sql.*;
import java.util.ArrayList;
import model.*;

/*
* Groupe 3_5-3 Esigelec 2020-2021
* class PersonneDAO
* permet de g�rer la table b3_personne
* */
/**
 * permet de g�rer la table personne
 * 
 * @author ESIGELEC Groupe 3_5-3
 * @version final
 * */
public class PersonneDAO extends ConnectionDAO {
	/**
	 * Constructor
	 * 
	 */
	public PersonneDAO() {
		super();
	}

	/**
	 * Permet d'ajouter une Personne dans la table b3_personne.
	 * Le mode est auto-commit par defaut : chaque insertion est validee
	 * 
	 * @param supplier la Personne a ajouter
	 * @return retourne le nombre de lignes ajoutees dans la table
	 */
	public int add(Personne supplier) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans l'insertion.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("INSERT INTO b3_personne (id, name, lastname, birth, fonction) VALUES (b3_personne_seq.nextval, ?, ?, ?, ?)");
			ps.setString(1, supplier.getName());
			ps.setString(2, supplier.getlastName());
			ps.setDate(3, supplier.getBirth());
			ps.setInt(4, supplier.getFonction());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Cet identifiant de personne existe d�j�. Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de modifier une personne dans la table b3_personne.
	 * 
	 * @param supplier le personne a modifier
	 * @return retourne le nombre de lignes modifiees dans la table
	 */
	public int update(Personne supplier) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans la modification.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("UPDATE b3_personne set name = ?, lastname = ?, birth = ?, fonction = ? WHERE id = ?");
			ps.setString(1, supplier.getName());
			ps.setString(2, supplier.getlastName());
			ps.setDate(3, supplier.getBirth());
			ps.setInt(4, supplier.getFonction());
			ps.setInt(5, supplier.getId());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de supprimer une personne dans la table b3_personne.
	 * 
	 * @param supplier la personne a supprimer
	 * @return retourne le nombre de lignes supprimees dans la table
	 */
	public int delete(Personne supplier) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID de personne
			ps = con.prepareStatement("DELETE FROM b3_personne WHERE id = ?");
			ps.setInt(1, supplier.getId());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-02292"))
				System.out.println("Ce fournisseur possede des articles, suppression impossible !"
						         + " Supprimer d'abord ses articles ou utiiser la m�thode de suppression avec articles.");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}


	/**
	 * Permet de recuperer une personne a partir de sa reference
	 * 
	 * @param reference la reference de personne a recuperer
	 * @return la personne si trouve;
	 * 			null si aucun fournisseur ne correspond a cette reference
	 */
	public Personne get(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Personne returnValue = null;

		// connexion a la base de donnees
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM b3_personne WHERE id = ?");
			ps.setInt(1, id);

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue = new Personne(rs.getInt("id"),
	                     			 	   rs.getString("name"),
	                     				   rs.getString("lastName"),
	                     				   rs.getDate("birth"),
	                     				   rs.getInt("Fonction"));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de recuperer tous les personnes stockes dans la table b3_personne
	 * 
	 * @return une ArrayList de personne
	 */
	public ArrayList<Personne> getList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Personne> returnValue = new ArrayList<Personne>();

		// connexion a la base de donnees
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM b3_personne ORDER BY id");

			// on execute la requete
			rs = ps.executeQuery();
			// on parcourt les lignes du resultat
			while (rs.next()) {
				returnValue.add(new Personne(rs.getInt("id"),
						                     rs.getString("name"),
						                     rs.getString("lastName"),
						                     rs.getDate("birth"),
											 rs.getInt("Fonction")));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	
}