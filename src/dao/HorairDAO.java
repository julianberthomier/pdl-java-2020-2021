package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.Horair;
/*
 * Groupe 3_5-3 Esigelec 2020-2021
 * class horairDAO
 * permet de g�rer la table b3_horair
 * */
/**
 * permet de g�rer la table horair
 * 
 * @author ESIGELEC Groupe 3_5-3
 * @version final
 * */
public class HorairDAO extends ConnectionDAO {
	public HorairDAO() {
		super();
	}

	/**
	 * Permet d'ajouter un horair dans la table b3_horair.
	 * 
	 * @param horair le horair a ajouter
	 * @return retourne le nombre de lignes ajoutees dans la table
	 */
	public int add(Horair horair) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans l'insertion.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("INSERT INTO b3_horair (id_profil, id_lieu, ouverture, fermeture) VALUES (?, ?, ?, ?)");
			ps.setInt(2, horair.getId_lieu());
			ps.setInt(1, horair.getId_profil());
			ps.setString(3, horair.getOuverture());
			ps.setString(4, horair.getFermeture());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Cet identifiant de fournisseur existe d�j�. Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de modifier un horair dans la table b3_horair.
	 * 
	 * @param horair le horair a modifier
	 * @return retourne le nombre de lignes modifiees dans la table
	 */
	public int update(Horair horair) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans la modification.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("UPDATE b3_horair set fermeture = ?, ouverture = ? WHERE id_profil = ? AND id_lieu = ? ");
			ps.setInt(4, horair.getId_lieu());
			ps.setInt(3, horair.getId_profil());
			ps.setString(2, horair.getOuverture());
			ps.setString(1, horair.getFermeture());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de supprimer un horair dans la table b3_horair.
	 * Si ce dernier possede des articles, la suppression n'a pas lieu.
	 * 
	 * @param horair l'horair a supprimer
	 * @return retourne le nombre de lignes supprimees dans la table
	 */
	public int delete(Horair horair) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID de horair
			ps = con.prepareStatement("DELETE FROM b3_horair WHERE id_profil = ?");
			ps.setInt(1, horair.getId_profil());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-02292"))
				System.out.println("Ce fournisseur possede des articles, suppression impossible !"
						         + " Supprimer d'abord ses articles ou utiiser la m�thode de suppression avec articles.");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}


	/**
	 * Permet de recuperer un horair a partir de sa reference
	 * 
	 * @param reference la reference de horair a recuperer
	 * @return l'horair si trouve;
	 * 			null si aucun fournisseur ne correspond a cette reference
	 */
	public Horair get(int id_lieu, int id_profil) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Horair returnValue = null;

		// connexion a la base de donnees
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM b3_horair WHERE id_lieu = ? AND id_profil = ?");
			ps.setInt(1, id_lieu);
			ps.setInt(2, id_profil);

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue = new Horair(rs.getInt("id_profil"),
	                     			 	   rs.getInt("id_lieu"),
	                     				   rs.getString("ouverture"),
	                     				   rs.getString("fermeture"));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de recuperer tous les horairs stockes dans la table b3_horair
	 * 
	 * @return une ArrayList de fournisseur
	 */
	public ArrayList<Horair> getList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Horair> returnValue = new ArrayList<Horair>();

		// connexion a la base de donnees
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM b3_horair ORDER BY id_profil");

			// on execute la requete
			rs = ps.executeQuery();
			// on parcourt les lignes du resultat
			while (rs.next()) {
				returnValue.add(new Horair(rs.getInt("id_profil"),
      			 	   						 rs.getInt("id_lieu"),
      			 	   						 rs.getString("ouverture"),
      			 	   						 rs.getString("fermeture")));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
}
