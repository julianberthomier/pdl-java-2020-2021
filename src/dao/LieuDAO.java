package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Lieu;
import model.Personne;
/*
* Groupe 3_5-3 Esigelec 2020-2021
* class LieuDAO
* permet de g�rer la table b3_lieu
* */
/**
 * permet de g�rer la table lieu
 * 
 * @author ESIGELEC Groupe 3_5-3
 * @version final
 * */
public class LieuDAO extends ConnectionDAO {

	public LieuDAO() {
		super();
	}

	/**
	 * Permet d'ajouter un lieu dans la table b3_lieu.
	 * 
	 * @param lieu le lieu a ajouter
	 * @return retourne le nombre de lignes ajoutees dans la table
	 */
	public int add(Lieu lieu) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans l'insertion.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("INSERT INTO b3_lieu (id_lieu, nom, location) VALUES (b3_lieu_seq.nextval, ?, ?)");
			ps.setString(1, lieu.getName_lieu());
			ps.setString(2, lieu.getLocation_lieu());
			

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Cet identifiant de lieu existe d�j�. Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de modifier un lieu dans la table b3_lieu.
	 * 
	 * @param lieu le lieu a modifier
	 * @return retourne le nombre de lignes modifiees dans la table
	 */
	public int update(Lieu lieu) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans la modification.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("UPDATE b3_lieu set nom = ?, location = ? WHERE id_lieu = ?");
			ps.setString(1, lieu.getName_lieu());
			ps.setString(2, lieu.getLocation_lieu());
			ps.setInt(3, lieu.getId_lieu());

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}


	


	/**
	 * Permet de recuperer un lieu a partir de sa reference
	 * 
	 * @param reference la reference du lieu a recuperer
	 * @return le lieu si trouve;
	 * 			null si aucun lieu ne correspond a cette reference
	 */
	public Lieu get(int id) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Lieu returnValue = null;

		// connexion a la base de donnees
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM b3_lieu WHERE id_lieu = ?");
			ps.setInt(1, id);

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue = new Lieu(rs.getInt("id_lieu"),
	                     			 	   rs.getString("nom"),
	                     				   rs.getString("location")
	                     				   );
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	/**
	 * Permet de recuperer tous les lieux stockes dans la table b3_lieu
	 * 
	 * @return une ArrayList de lieu
	 */
	public ArrayList<Lieu> getList() {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Lieu> returnValue = new ArrayList<Lieu>();

		// connexion a la base de donnees
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM b3_lieu ORDER BY id_lieu");

			// on execute la requete
			rs = ps.executeQuery();
			// on parcourt les lignes du resultat
			while (rs.next()) {
				returnValue.add(new Lieu(rs.getInt("id_lieu"),
						                     rs.getString("nom"),
						                     rs.getString("location")));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}


	
}
