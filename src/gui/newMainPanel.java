package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import net.miginfocom.swing.MigLayout;
import oracle.sql.DATE;

import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import java.awt.List;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import dao.*;
import model.*;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Panel;
import java.awt.Button;
import model.Personne;
import java.sql.Date;
import java.time.format.DateTimeFormatter;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.HierarchyEvent;
/*
* Groupe 3_5-3 Esigelec 2020-2021
* class ProfilDAO
* Main & interface
* */
public class newMainPanel {
	//liste pour toute les class
	ArrayList<Personne> liste;
	ArrayList<Lieu> liste2;
	ArrayList<Profil> liste_profil;
	ArrayList<Horair> liste_horair;
	ArrayList<Badge> liste_badge;
//panel 
	JPanel panel_1;
	JPanel panel;
	JPanel panel2;
	JPanel panel_badge;
	JPanel panel_lieu;
	JPanel panel_profil;
	JPanel panel_horair;
// texte
	JLabel lblName;
	JLabel lblLastName;
	JLabel lblBirthday;
	JLabel lblFonction;

	// le texte modifiable

	private JTextField textFieldName;
	private JTextField textFieldLastName;
	private JTextField textFieldBirthday_j;
//main frame
	private JFrame frame;
	
//text field
	private JTextField jtname_lieu;
	private JTextField jtlocation;
	private JTextField jtname_profil;
	private JTextField textField_ouverture;
	private JTextField textField_fermeture;
	private JTextField textFieldBirthday_m;
	private JTextField textFieldBirthday_a;
//declaration de mes classes
	PersonneDAO personneDAO;
	Personne personne;
	LieuDAO lieuDAO;
	Lieu lieu;
	ProfilDAO profilDAO;
	Profil profil;
	Profil profil_save;
	HorairDAO horairDAO;
	Horair horair;
	BadgeDAO badgeDAO;
	Badge badge;
//combo box
	JComboBox comboBox_badge;
//liste
	List list;
//bouton
	JButton supprimer;
	JButton modifier;
	private JButton btnhorair;
//pour les return des class dao
	int returnValue;
	//sauvegarde  de choix ou donner en int
	int choice = 0;
	int select = 0;
	int id_save = 0;



	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					newMainPanel window = new newMainPanel();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public newMainPanel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//creation des frames
		frame = new JFrame();
		frame.setBounds(100, 100, 670, 503);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBounds(144, 10, 502, 446);
		panel.setLayout(null);
		frame.getContentPane().add(panel);

		panel_lieu = new JPanel();
		panel_lieu.setBounds(144, 10, 502, 446);
		panel_lieu.setLayout(null);
		frame.getContentPane().add(panel_lieu);

		panel_profil = new JPanel();
		panel_profil.setBounds(144, 10, 502, 446);
		panel_profil.setLayout(null);
		frame.getContentPane().add(panel_profil);

		panel_horair = new JPanel();
		panel_horair.setBounds(144, 10, 502, 446);
		panel_horair.setLayout(null);
		frame.getContentPane().add(panel_horair);

		panel_badge = new JPanel();
		panel_badge.setBounds(144, 10, 502, 446);
		panel_badge.setLayout(null);
		frame.getContentPane().add(panel_badge);
		
		//creation des �l�ments sur les frames
		comboBox_badge = new JComboBox();
		comboBox_badge.setBounds(157, 193, 189, 21);
		panel_badge.add(comboBox_badge);

		JButton btnvalider_badge = new JButton("valider");
		btnvalider_badge.setBounds(206, 253, 85, 21);
		panel_badge.add(btnvalider_badge);

		JComboBox comboBox_lieu = new JComboBox();
		comboBox_lieu.setBounds(166, 23, 189, 21);
		panel_horair.add(comboBox_lieu);

		List list_horair = new List();
		list_horair.setBounds(62, 140, 388, 220);
		panel_horair.add(list_horair);

		textField_ouverture = new JTextField();
		textField_ouverture.setBounds(88, 81, 96, 19);
		panel_horair.add(textField_ouverture);
		textField_ouverture.setColumns(10);

		textField_fermeture = new JTextField();
		textField_fermeture.setBounds(278, 81, 96, 19);
		panel_horair.add(textField_fermeture);
		textField_fermeture.setColumns(10);

		JLabel lblouverture = new JLabel("ouverture");
		lblouverture.setBounds(25, 84, 55, 13);
		panel_horair.add(lblouverture);

		JLabel lblfermeture = new JLabel("fermeture");
		lblfermeture.setBounds(205, 84, 55, 13);
		panel_horair.add(lblfermeture);

		JButton ajouter_horair = new JButton("ajouter");
		ajouter_horair.setBounds(392, 80, 85, 21);
		panel_horair.add(ajouter_horair);

		JButton supprimer_horair = new JButton("supprimer");
		supprimer_horair.setBounds(62, 384, 85, 21);
		panel_horair.add(supprimer_horair);

		JButton btnvalider_horair = new JButton("valider");
		btnvalider_horair.setBounds(365, 384, 85, 21);
		panel_horair.add(btnvalider_horair);

		jtname_profil = new JTextField();
		jtname_profil.setBounds(198, 157, 149, 19);
		panel_profil.add(jtname_profil);
		jtname_profil.setColumns(10);

		JLabel lblName_profil = new JLabel("nom du profil");
		lblName_profil.setBounds(104, 160, 84, 13);
		panel_profil.add(lblName_profil);

		JButton valider_profil = new JButton("valider");
		valider_profil.setBounds(201, 310, 85, 21);
		panel_profil.add(valider_profil);

		jtname_lieu = new JTextField();
		jtname_lieu.setBounds(178, 145, 149, 19);
		panel_lieu.add(jtname_lieu);
		jtname_lieu.setColumns(10);

		jtlocation = new JTextField();
		jtlocation.setBounds(178, 206, 149, 19);
		panel_lieu.add(jtlocation);
		jtlocation.setColumns(10);

		JLabel lblName_lieu = new JLabel("nom du lieu");
		lblName_lieu.setBounds(84, 148, 84, 13);
		panel_lieu.add(lblName_lieu);

		JLabel lbllocation = new JLabel("location");
		lbllocation.setBounds(84, 209, 62, 13);
		panel_lieu.add(lbllocation);

		JButton ajouter = new JButton("ajouter");
		ajouter.setBounds(42, 58, 85, 21);
		panel.add(ajouter);

		JButton valider_lieu = new JButton("valider");
		valider_lieu.setBounds(201, 310, 85, 21);
		panel_lieu.add(valider_lieu);

		list = new List();
		list.setBounds(29, 107, 440, 282);
		panel.add(list);

		modifier = new JButton("modifier");
		modifier.setBounds(289, 415, 85, 21);
		panel.add(modifier);

		supprimer = new JButton("supprimer");
		supprimer.setBounds(384, 415, 85, 21);
		panel.add(supprimer);

		btnhorair = new JButton("horaire");
		btnhorair.setBounds(194, 415, 85, 21);
		panel.add(btnhorair);

		panel2 = new JPanel();
		panel2.setBounds(144, 10, 502, 446);
		panel2.setLayout(null);
		frame.getContentPane().add(panel2);

		JComboBox comboBox_fonction = new JComboBox();
		comboBox_fonction.setBounds(134, 229, 183, 21);
		panel2.add(comboBox_fonction);

		lblName = new JLabel("name");
		lblName.setBounds(69, 131, 55, 13);
		panel2.add(lblName);

		lblLastName = new JLabel("lastname");
		lblLastName.setBounds(69, 166, 65, 13);
		panel2.add(lblLastName);

		lblBirthday = new JLabel("birthday");
		lblBirthday.setBounds(69, 197, 65, 13);
		panel2.add(lblBirthday);

		lblFonction = new JLabel("fonction");
		lblFonction.setBounds(69, 232, 55, 13);
		panel2.add(lblFonction);

		textFieldName = new JTextField();
		textFieldName.setBounds(134, 127, 183, 21);
		textFieldName.setColumns(10);
		panel2.add(textFieldName);

		textFieldLastName = new JTextField();
		textFieldLastName.setColumns(10);
		textFieldLastName.setBounds(134, 162, 183, 21);
		panel2.add(textFieldLastName);

		textFieldBirthday_j = new JTextField();
		textFieldBirthday_j.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textFieldBirthday_j.getText().length() >= 2) // limit textfield to 2 characters
					e.consume();
			}
		});
		textFieldBirthday_j.setColumns(10);
		textFieldBirthday_j.setBounds(144, 193, 26, 21);
		panel2.add(textFieldBirthday_j);

		Button valider = new Button("valider");
		valider.setBounds(217, 352, 66, 21);
		panel2.add(valider);

		JLabel lbldate = new JLabel("(jj/mm/aaaa)");
		lbldate.setBounds(272, 197, 92, 13);
		panel2.add(lbldate);

		textFieldBirthday_m = new JTextField();
		textFieldBirthday_m.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textFieldBirthday_m.getText().length() >= 2) // limit textfield to 2 characters
					e.consume();
			}
		});
		textFieldBirthday_m.setColumns(10);
		textFieldBirthday_m.setBounds(180, 193, 26, 21);
		panel2.add(textFieldBirthday_m);

		textFieldBirthday_a = new JTextField();
		textFieldBirthday_a.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (textFieldBirthday_a.getText().length() >= 4) // limit textfield to 4 characters
					e.consume();
			}
		});
		textFieldBirthday_a.setColumns(10);
		textFieldBirthday_a.setBounds(217, 193, 45, 21);
		panel2.add(textFieldBirthday_a);

		JLabel lbldecodate = new JLabel(" /");
		lbldecodate.setBounds(170, 197, 45, 13);
		panel2.add(lbldecodate);

		JLabel lbldecodate2 = new JLabel("  /");
		lbldecodate2.setBounds(205, 197, 45, 13);
		panel2.add(lbldecodate2);

		panel_1 = new JPanel();
		panel_1.setBounds(10, 10, 134, 446);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JMenuItem personnebtn = new JMenuItem("personne");
		personnebtn.setBackground(Color.WHITE);
		personnebtn.setBounds(0, 34, 133, 24);
		panel_1.add(personnebtn);

		JMenuItem badge_menu = new JMenuItem("badge");
		badge_menu.setBounds(0, 70, 133, 24);
		panel_1.add(badge_menu);

		JMenuItem lieu_menu = new JMenuItem("lieu");
		lieu_menu.setBounds(0, 104, 133, 24);
		panel_1.add(lieu_menu);

		JMenuItem btnprofil = new JMenuItem("profil");
		btnprofil.setBounds(0, 138, 133, 24);
		panel_1.add(btnprofil);

		disable();

		// menu personne bouton
		personnebtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				disable();
				//vide l'affichage de la liste
				list.removeAll();
				//affiche les panel de personne
				panel.setVisible(true);
				panel.setEnabled(true);
				personneDAO = new PersonneDAO();
				liste = personneDAO.getList();
				//affichage des personne existante
				for (Personne s : liste) {
					// appel explicite de la methode toString de la classe Object (a privilegier)
					list.add(s.getName() + " " + s.getlastName());
				}
				//quelle menu a �t� choisie
				select = 1;

			}
		});
		// menu badge bouton
		badge_menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//meme principe que pour personne
				disable();
				select = 4;
				list.removeAll();
				panel.setVisible(true);
				panel.setEnabled(true);
				personneDAO = new PersonneDAO();
				liste = personneDAO.getList();
				badgeDAO = new BadgeDAO();
				liste_badge = badgeDAO.getList();
				//affichage des personne ayant d�ja un badge
				for (Badge s : liste_badge) {
					for (Personne z : liste) {
						if (s.getId_personne() == z.getId()) {
							list.add(s.getId_badge() + " " + z.getName());
						}
					}
				}
				modifier.setEnabled(false);
			}
		});
		// menu lieu bouton
		lieu_menu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//m�me principe que personne
				disable();
				list.removeAll();
				panel.setVisible(true);
				panel.setEnabled(true);
				lieuDAO = new LieuDAO();
				liste2 = lieuDAO.getList();
				for (Lieu s : liste2) {
					// appel explicite de la methode toString de la classe Object (a privilegier)
					list.add(s.getName_lieu() + " " + s.getLocation_lieu());
				}
				select = 3;
				supprimer.setEnabled(false);
			}
		});
		// menu badge bouton
		btnprofil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//m�me principe que personne
				disable();
				list.removeAll();
				panel.setVisible(true);
				panel.setEnabled(true);
				btnhorair.setVisible(true);
				btnhorair.setEnabled(true);
				profilDAO = new ProfilDAO();
				liste_profil = profilDAO.getList();
				for (Profil s : liste_profil) {
					// appel explicite de la methode toString de la classe Object (a privilegier)
					list.add(s.getName_profil());
				}
				select = 2;
			}
		});

		// validation personne

		valider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (choice == 1) {
					//creation d'une personne a partir de donner renter
					String name = textFieldName.getText();
					String lastName = textFieldLastName.getText();
					//changement de texte en date sql
					String date = textFieldBirthday_a.getText() + "-" + textFieldBirthday_m.getText() + "-"
							+ textFieldBirthday_j.getText();
					Date Birthday = Date.valueOf(date);
					int fonction = liste_profil.get(comboBox_fonction.getSelectedIndex()).getId_profil();
					personne = new Personne(1, name, lastName, Birthday, fonction);
					personneDAO = new PersonneDAO();
					//enregistrement dans la base de donn�e
					returnValue = personneDAO.add(personne);
					//verification
					System.out.println(returnValue + " personne ajoute");

				}
				if (choice == 2) {
					//modification d'une personne a partir de donner renter
					String name = textFieldName.getText();
					String lastName = textFieldLastName.getText();
					String date = textFieldBirthday_a.getText() + "-" + textFieldBirthday_m.getText() + "-"
							+ textFieldBirthday_j.getText();
					Date Birthday = Date.valueOf(date);
					int fonction = liste_profil.get(comboBox_fonction.getSelectedIndex()).getId_profil();
					liste = personneDAO.getList();
					//par rapport a l'�l�ment selectionn�
					personne = new Personne(liste.get(list.getSelectedIndex()).getId(), name, lastName, Birthday,
							fonction);
					personneDAO = new PersonneDAO();
					returnValue = personneDAO.update(personne);
					System.out.println(returnValue + " personne modifier");
				}
				disable();
			}
		});
		// validation lieu

		valider_lieu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (choice == 1) {
					//m�me chose que pour personne
					String name = jtname_lieu.getText();
					String location = jtlocation.getText();
					lieu = new Lieu(1, name, location);
					lieuDAO = new LieuDAO();
					returnValue = lieuDAO.add(lieu);
					System.out.println(returnValue + " lieu ajoute");

				}
				if (choice == 2) {
					//m�me chose que pour personne
					String name = jtname_lieu.getText();
					String location = jtlocation.getText();
					liste2 = lieuDAO.getList();
					lieu = new Lieu(liste2.get(list.getSelectedIndex()).getId_lieu(), name, location);
					lieuDAO = new LieuDAO();
					returnValue = lieuDAO.update(lieu);
					System.out.println(returnValue + " lieu modifier");
				}
				disable();
			}
		});

		// validation profil
		valider_profil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (choice == 1) {
					//m�me chose que pour personne
					String name = jtname_profil.getText();
					profil = new Profil(1, name);
					profilDAO = new ProfilDAO();
					returnValue = profilDAO.add(profil);
					System.out.println(returnValue + " profil ajoute");

				}
				if (choice == 2) {
					//m�me chose que pour personne
					String name = jtname_profil.getText();
					liste_profil = profilDAO.getList();
					profil = new Profil(liste_profil.get(list.getSelectedIndex()).getId_profil(), name);
					profilDAO = new ProfilDAO();
					returnValue = profilDAO.update(profil);
					System.out.println(returnValue + " profil modifier");
				}
				disable();
			}
		});

		// validation horaire
		btnvalider_horair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//m�me chose que pour personne
				horairDAO = new HorairDAO();
				Horair horair_save = new Horair(profil_save.getId_profil(), 0, "", "");
				horairDAO.delete(horair_save);
				for (Horair s : liste_horair) {
					if (profil_save.getId_profil() == s.getId_profil()) {
						horairDAO.add(s);
					}
				}
				disable();
			}
		});

		// validation badge
		btnvalider_badge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//m�me chose que pour personne
				badge = new Badge(id_save);
				badgeDAO = new BadgeDAO();
				returnValue = badgeDAO.add(badge);
				System.out.println(returnValue + " badge ajoute");
				disable();
			}
		});
		// menu 2 ajouter un elemnt
		ajouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switch (select) {
				//affichage du menu selectionn�
				case 1:
					disable();
					panel2.setVisible(true);
					panel2.setEnabled(true);
					//mise a 0 des texte field
					textFieldName.setText("");
					comboBox_fonction.removeAllItems();
					textFieldBirthday_j.setText("");
					textFieldBirthday_a.setText("");
					textFieldBirthday_m.setText("");
					textFieldLastName.setText("");
					profilDAO = new ProfilDAO();
					liste_profil = profilDAO.getList();
					//ajout des profiles a la combobox
					for (Profil s : liste_profil) {
						comboBox_fonction.addItem(s.getName_profil());

					}
					choice = 1;
					break;

				case 2:
					disable();
					panel_profil.setVisible(true);
					panel_profil.setEnabled(true);
					choice = 1;
					jtname_profil.setText("");
					break;

				case 3:
					disable();
					panel_lieu.setVisible(true);
					panel_lieu.setEnabled(true);
					jtname_lieu.setText("");
					jtlocation.setText("");
					choice = 1;
					break;
				case 4:
					disable();
					boolean badgegood;
					panel_badge.setVisible(true);
					panel_badge.setEnabled(true);
					personneDAO = new PersonneDAO();
					liste = personneDAO.getList();
					badgeDAO = new BadgeDAO();
					liste_badge = badgeDAO.getList();
					comboBox_badge.removeAllItems();
					//ajouter des personne a la combobox si la personne n'a pas de badge
					for (Personne z : liste) {
						badgegood = false;
						for (Badge s : liste_badge) {
							if (s.getId_personne() == z.getId()) {
								badgegood = true;
							}
						}
						if (badgegood == false) {
							comboBox_badge.addItem(z.getId() + " " + z.getName());
							id_save = z.getId();
						}

					}
					break;

				default:
					//en cas d'erreur
					System.out.println("erreur : impossible de ajouter");

				}
			}
		});

		modifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switch (select) {
				//affichage du menu selectionn�
				case 1:
					disable();
					panel2.setVisible(true);
					panel2.setEnabled(true);
					//remplis les textfield avec les donn� li�
					textFieldName.setText(liste.get(list.getSelectedIndex()).getName());
					comboBox_fonction.removeAllItems();
					final String SEPARATEUR = "-";
					String date[] = String.valueOf(liste.get(list.getSelectedIndex()).getBirth()).split(SEPARATEUR);
					textFieldBirthday_a.setText(date[0]);
					textFieldBirthday_m.setText(date[1]);
					textFieldBirthday_j.setText(date[2]);
					textFieldLastName.setText(liste.get(list.getSelectedIndex()).getlastName());
					profilDAO = new ProfilDAO();
					liste_profil = profilDAO.getList();
					for (Profil s : liste_profil) {
						comboBox_fonction.addItem(s.getName_profil());
						if (s.getId_profil() == liste.get(list.getSelectedIndex()).getFonction()) {
							comboBox_fonction.setSelectedItem(s.getName_profil());
						}

					}
					choice = 2;
					break;

				case 2:
					disable();
					panel_profil.setVisible(true);
					panel_profil.setEnabled(true);
					jtname_profil.setText(liste_profil.get(list.getSelectedIndex()).getName_profil());
					choice = 2;
					break;

				case 3:
					disable();
					panel_lieu.setVisible(true);
					panel_lieu.setEnabled(true);
					jtname_lieu.setText(liste2.get(list.getSelectedIndex()).getName_lieu());
					jtlocation.setText(liste2.get(list.getSelectedIndex()).getLocation_lieu());
					choice = 2;
					break;
				default:
					System.out.println("erreur : impossible de modifier");

				}
			}
		});

		supprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				switch (select) {
				//supprimer l'�l�ment de la list et de la base de donn�e
				case 1:
					personneDAO = new PersonneDAO();
					//cration de l'�l�ment a supprimer
					String name = liste.get(list.getSelectedIndex()).getName();
					String lastName = liste.get(list.getSelectedIndex()).getlastName();
					Date Birthday = liste.get(list.getSelectedIndex()).getBirth();
					int fonction = liste.get(list.getSelectedIndex()).getFonction();
					personne = new Personne(liste.get(list.getSelectedIndex()).getId(), name, lastName, Birthday,
							fonction);
					//suppression
					returnValue = personneDAO.delete(personne);
					//retait et r�affichage de la liste
					liste = personneDAO.getList();
					list.remove(list.getSelectedIndex());
					System.out.println(returnValue + " personne supprimer");
					break;

				case 2:
					profilDAO = new ProfilDAO();
					profil = new Profil(liste_profil.get(list.getSelectedIndex()).getId_profil(), "");
					returnValue = profilDAO.delete(profil);
					liste_profil = profilDAO.getList();
					list.remove(list.getSelectedIndex());
					System.out.println(returnValue + " fournisseur supprimer");
					break;

				case 4:
					badgeDAO = new BadgeDAO();
					badge = new Badge(liste_badge.get(list.getSelectedIndex()).getId_personne());
					returnValue = badgeDAO.delete(badge);
					liste_badge = badgeDAO.getList();
					list.remove(list.getSelectedIndex());
					System.out.println(returnValue + " badge supprimer");
					break;

				default:
					System.out.println("erreur : impossible de supprimer");
				}
			}
		});
		//affichage du menu horair / event bouton
		btnhorair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//permet d'interagir avec les horaires d'un profil choisi
				if (list.getSelectedIndex() != -1) {
					//r�initialisation des �l�ment
					comboBox_lieu.removeAllItems();
					list_horair.removeAll();
					profil_save = liste_profil.get(list.getSelectedIndex());
					disable();
					panel_horair.setVisible(true);
					panel_horair.setEnabled(true);
					lieuDAO = new LieuDAO();
					liste2 = lieuDAO.getList();
					horairDAO = new HorairDAO();
					liste_horair = horairDAO.getList();
					//ajout des lieux a la combobox
					for (Lieu s : liste2) {
						comboBox_lieu.addItem(s.getName_lieu() + " " + s.getLocation_lieu());
					}
					list.removeAll();
					System.out.println("save" + profil_save.getId_profil());
				}
			}
		});

		ajouter_horair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//ajouer un horair sur une combinaison lieu profil qui n'existe pas d�j�
				Horair horairp = new Horair(profil_save.getId_profil(), comboBox_lieu.getSelectedIndex(),
						textField_ouverture.getText(), textField_fermeture.getText());
				boolean exist = false;
				for (Horair s : liste_horair) {
					if (s.getId_lieu() == comboBox_lieu.getSelectedIndex()
							&& s.getId_profil() == profil_save.getId_profil())
						exist = true;
				}
				if (!exist) {
					liste_horair.add(horairp);

					System.out.println("profil id  " + horairp.getId_profil());
				}
				list_horair.removeAll();
				for (Horair s : liste_horair) {
					if (profil_save.getId_profil() == s.getId_profil()) {
						list_horair.add(profil_save.getName_profil() + " " + liste2.get(s.getId_lieu()).getName_lieu()
								+ " " + s.getOuverture() + " " + s.getFermeture());
						if (comboBox_lieu.getSelectedIndex() == s.getId_lieu()) {
							textField_ouverture.setText("allready exist");
							textField_fermeture.setText("allready exist");
							textField_ouverture.setEditable(false);
							textField_fermeture.setEditable(false);

						}
					}
				}
			}
		});

		supprimer_horair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//supprimez horair
				Horair horait_save = null;
				for (Horair s : liste_horair) {
					if (profil_save.getId_profil() == s.getId_profil()
							&& comboBox_lieu.getSelectedIndex() == s.getId_lieu()) {
						System.out.println(s.getId_lieu());
						horait_save = s;
						textField_ouverture.setEditable(true);
						textField_fermeture.setEditable(true);
						textField_ouverture.setText("");
						textField_fermeture.setText("");
					}
				}
				System.out.println(comboBox_lieu.getSelectedIndex());
				liste_horair.remove(horait_save);
				list_horair.removeAll();
				for (Horair s : liste_horair) {
					// System.out.println(s.getId_profil());
					if (profil_save.getId_profil() == s.getId_profil()) {
						list_horair.add(profil_save.getName_profil() + " " + liste2.get(s.getId_lieu()).getName_lieu()
								+ " " + s.getOuverture() + " " + s.getFermeture());
					}
				}
			}
		});

		comboBox_lieu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//affichage de l'horair d'un lieu et d'un profil
				list_horair.removeAll();
				textField_ouverture.setEditable(true);
				textField_fermeture.setEditable(true);
				textField_ouverture.setText("");
				textField_fermeture.setText("");
				for (Horair s : liste_horair) {
					// System.out.println(s.getId_profil());
					if (profil_save.getId_profil() == s.getId_profil()) {
						list_horair.add(profil_save.getName_profil() + " " + liste2.get(s.getId_lieu()).getName_lieu()
								+ " " + s.getOuverture() + " " + s.getFermeture());
						if (comboBox_lieu.getSelectedIndex() == s.getId_lieu()) {
							textField_ouverture.setText("allready exist");
							textField_fermeture.setText("allready exist");
							textField_ouverture.setEditable(false);
							textField_fermeture.setEditable(false);

						}
					}
				}
			}
		});

	}

	public void disable() {
		//r�initialisation (d�sactivation de tout les frame)
		textField_ouverture.setEditable(true);
		textField_fermeture.setEditable(true);
		supprimer.setEnabled(true);
		modifier.setEnabled(true);
		btnhorair.setVisible(false);
		btnhorair.setEnabled(false);
		panel.setVisible(false);
		panel.setEnabled(false);
		panel2.setVisible(false);
		panel2.setEnabled(false);
		panel_lieu.setVisible(false);
		panel_lieu.setEnabled(false);
		panel_profil.setVisible(false);
		panel_profil.setEnabled(false);
		panel_horair.setVisible(false);
		panel_horair.setEnabled(false);
		panel_badge.setVisible(false);
		panel_badge.setEnabled(false);

	}
}
