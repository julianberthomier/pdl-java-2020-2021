package model;
/*
 * Groupe 3_5-3 Esigelec 2020-2021
 * class horair
 * */
/**
 * class horair
 * 
 * @author ESIGELEC Groupe 3_5-3
 * @version final
 * */
public class Horair {
	/** 
	 * reference de profil
	 */
int id_profil;
/** 
 * reference de lieu
 */
int id_lieu;
/** 
 * horair d'ouverture
 */
String ouverture;
/** 
 * horair de fermeture
 */
String fermeture;

public Horair(int id_profil, int id_lieu, String ouverture, String fermeture) {
	this.id_profil = id_profil;
	this.id_lieu = id_lieu;
	this.ouverture = ouverture;
	this.fermeture = fermeture;
}

public int getId_profil() {
	return id_profil;
}

public void setId_profil(int id_profil) {
	this.id_profil = id_profil;
}

public int getId_lieu() {
	return id_lieu;
}

public void setId_lieu(int id_lieu) {
	this.id_lieu = id_lieu;
}

public String getOuverture() {
	return ouverture;
}

public void setOuverture(String ouverture) {
	this.ouverture = ouverture;
}

public String getFermeture() {
	return fermeture;
}

public void setFermeture(String fremeture) {
	this.fermeture = fremeture;
}


}
