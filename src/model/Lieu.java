package model;
/*
 * Groupe 3_5-3 Esigelec 2020-2021
 * class lieu
 * */
/**
 * class lieu
 * 
 * @author ESIGELEC Groupe 3_5-3
 * @version final
 * */
public class Lieu {
	/** 
	 * reference de lieu
	 */
int id_lieu;
/** 
 * nom du lieu
 */
String name_lieu;
/** 
 * location du lieu
 */
String location_lieu;

public Lieu(int id_lieu , String name_lieu, String location_lieu) {
	this.id_lieu = id_lieu;
	this.name_lieu = name_lieu;
	this.location_lieu = location_lieu;
}

public int getId_lieu() {
	return id_lieu;
}

public void setId_lieu(int id_lieu) {
	this.id_lieu = id_lieu;
}

public String getName_lieu() {
	return name_lieu;
}

public void setName_lieu(String name_lieu) {
	this.name_lieu = name_lieu;
}

public String getLocation_lieu() {
	return location_lieu;
}

public void setLocation_lieu(String location_lieu) {
	this.location_lieu = location_lieu;
}


}
