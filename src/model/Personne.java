package model;
import java.sql.Date;
//import java.util.ArrayList;

/*
 * Groupe 3_5-3 Esigelec 2020-2021
 * class personne
 * */
/**
 * class personne
 * 
 * @author ESIGELEC Groupe 3_5-3
 * @version final
 * */
public class Personne {
	/** 
	 * reference de personne
	 */
	private int id;		
	/**
	 * nom
	 */
	private String name;	
	/**
	 * prenom
	 */
	private String lastName;		
	/**
	 * date de naissance
	 */
	private Date birth;
	/**
	 * fonction
	 */
	private int fonction;
	
	
	public Personne(int id, String name, String lastName, Date birth,int fonction) {
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.birth = birth;
		this.fonction = fonction;
	}
	

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getlastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}
	public int getFonction() {
		return fonction;
	}

	public void setFonction(int fonction) {
		this.fonction = fonction;
	}
	

	public String toString() {
		return "personne [ref : " + id + ", " + name
				+ ", " + lastName + ", " + birth +"," + fonction  + "]";
	}
}
