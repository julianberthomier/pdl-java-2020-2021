package model;

/*
 * Groupe 3_5-3 Esigelec 2020-2021
 * class profil
 * */
/**
 * class profil
 * 
 * @author ESIGELEC Groupe 3_5-3
 * @version final
 * */
public class Profil {
	/** 
	 * reference du profil
	 */
	int id_profil;
	/** 
	 * nom du profil
	 */
	String name_profil;

	public Profil(int id_profil, String name_profil) {
		this.id_profil = id_profil;
		this.name_profil = name_profil;
	}

	public int getId_profil() {
		return id_profil;
	}

	public void setId_profil(int id_profil) {
		this.id_profil = id_profil;
	}

	public String getName_profil() {
		return name_profil;
	}

	public void setName_profil(String name_profil) {
		this.name_profil = name_profil;
	}

}
