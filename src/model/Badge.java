package model;
/*
 * Groupe 3_5-3 Esigelec 2020-2021
 * class badge
 * */
/**
 * class badge
 * 
 * @author ESIGELEC Groupe 3_5-3
 * @version final
 * */
public class Badge {
	/** 
	 * reference de badge
	 */
int id_badge;
/** 
 * reference de personne
 */
int id_personne;

public Badge( int id_personne) {
	this.id_personne = id_personne;
}

public Badge(int id_badge, int id_personne) {
	this.id_badge = id_badge;
	this.id_personne = id_personne;
}

public int getId_badge() {
	return id_badge;
}
public void setId_badge(int id_badge) {
	this.id_badge = id_badge;
}
public int getId_personne() {
	return id_personne;
}
public void setId_personne(int id_personne) {
	this.id_personne = id_personne;
}


}
